#!/bin/sh
# This information comes from swift/utils/update_checkout/update-checkout-config.json

set -e

readonly SWIFTTAG=5.10.1
readonly YAMS_VERSION=5.0.1
readonly SWIFT_ARGUMENT_PARSER_VERSION=1.2.3
readonly SWIFT_CRYPTO_VERSION=3.0.0
readonly SWIFT_ATOMICS_VERSION=1.0.2
readonly SWIFT_COLLECTIONS_VERSION=1.0.5
readonly SWIFT_NUMERICS_VERSION=1.0.1
readonly SWIFT_SYSTEM_VERSION=1.1.1
readonly SWIFT_NIO_VERSION=2.31.2
readonly SWIFT_NIO_SSL_VERSION=2.15.0
readonly SWIFT_FORMAT_VERSION=510.1.0
readonly SWIFT_CERTIFICATES_VERSION=1.0.1
readonly SWIFT_ASN1_VERSION=1.0.0

if [ -f "swiftlang_$SWIFTTAG.orig.tar.xz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig.tar.xz"
    exit
fi

rm -rf swift-repack
mkdir -p swift-repack
cd swift-repack
mkdir -p swiftlang-$SWIFTTAG
cd swiftlang-$SWIFTTAG


if [ -f "../swiftlang_$SWIFTTAG.orig-swift.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift.tar.gz"
else
    wget https://github.com/swiftlang/swift/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift.tar.gz
fi
mkdir swift
tar -C swift -zxf ../swiftlang_$SWIFTTAG.orig-swift.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-corelibs-libdispatch.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-corelibs-libdispatch.tar.gz"
else
    wget https://github.com/apple/swift-corelibs-libdispatch/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-corelibs-libdispatch.tar.gz
fi
mkdir swift-corelibs-libdispatch
tar -C swift-corelibs-libdispatch -xzf ../swiftlang_$SWIFTTAG.orig-swift-corelibs-libdispatch.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-corelibs-foundation.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-corelibs-foundation.tar.gz"
else
    wget https://github.com/swiftlang/swift-corelibs-foundation/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-corelibs-foundation.tar.gz
fi
mkdir swift-corelibs-foundation
tar -C swift-corelibs-foundation -xzf ../swiftlang_$SWIFTTAG.orig-swift-corelibs-foundation.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-integration-tests.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-integration-tests.tar.gz"
else
    wget https://github.com/swiftlang/swift-integration-tests/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-integration-tests.tar.gz
fi
mkdir swift-integration-tests
tar -C swift-integration-tests -xzf ../swiftlang_$SWIFTTAG.orig-swift-integration-tests.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-corelibs-xctest.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-corelibs-xctest.tar.gz"
else
    wget https://github.com/swiftlang/swift-corelibs-xctest/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-corelibs-xctest.tar.gz
fi
mkdir swift-corelibs-xctest
tar -C swift-corelibs-xctest -xzf ../swiftlang_$SWIFTTAG.orig-swift-corelibs-xctest.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swiftpm.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swiftpm.tar.gz"
else
    wget https://github.com/swiftlang/swift-package-manager/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swiftpm.tar.gz
fi
mkdir swiftpm
tar -C swiftpm -xzf ../swiftlang_$SWIFTTAG.orig-swiftpm.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-llbuild.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-llbuild.tar.gz"
else
    wget https://github.com/swiftlang/swift-llbuild/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-llbuild.tar.gz
fi
mkdir llbuild
tar -C llbuild -xzf ../swiftlang_$SWIFTTAG.orig-llbuild.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-cmark.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-cmark.tar.gz"
else
    wget https://github.com/swiftlang/swift-cmark/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-cmark.tar.gz
fi
mkdir cmark
tar -C cmark -xzf ../swiftlang_$SWIFTTAG.orig-cmark.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-xcode-playground-support.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-xcode-playground-support.tar.gz"
else
    wget https://github.com/apple/swift-xcode-playground-support/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-xcode-playground-support.tar.gz
fi
mkdir swift-xcode-playground-support
tar -C swift-xcode-playground-support -xzf ../swiftlang_$SWIFTTAG.orig-swift-xcode-playground-support.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-sourcekit-lsp.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-sourcekit-lsp.tar.gz"
else
    wget https://github.com/swiftlang/sourcekit-lsp/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-sourcekit-lsp.tar.gz
fi
mkdir sourcekit-lsp
tar -C sourcekit-lsp -xzf ../swiftlang_$SWIFTTAG.orig-sourcekit-lsp.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-indexstore-db.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-indexstore-db.tar.gz"
else
    wget https://github.com/swiftlang/indexstore-db/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-indexstore-db.tar.gz
fi
mkdir indexstore-db
tar -C indexstore-db -xzf ../swiftlang_$SWIFTTAG.orig-indexstore-db.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-llvm-project.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-llvm-project.tar.gz"
else
    wget https://github.com/swiftlang/llvm-project/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-llvm-project.tar.gz
fi
mkdir llvm-project
tar -C llvm-project -xzf ../swiftlang_$SWIFTTAG.orig-llvm-project.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-tools-support-core.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-tools-support-core.tar.gz"
else
    wget https://github.com/swiftlang/swift-tools-support-core/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-tools-support-core.tar.gz
fi
mkdir swift-tools-support-core
tar -C swift-tools-support-core -xzf ../swiftlang_$SWIFTTAG.orig-swift-tools-support-core.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-argument-parser.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-argument-parser.tar.gz"
else
    wget https://github.com/apple/swift-argument-parser/archive/refs/tags/$SWIFT_ARGUMENT_PARSER_VERSION.tar.gz
    mv -f $SWIFT_ARGUMENT_PARSER_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-argument-parser.tar.gz
fi
mkdir swift-argument-parser
tar -C swift-argument-parser -xzf ../swiftlang_$SWIFTTAG.orig-swift-argument-parser.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-driver.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-driver.tar.gz"
else
    wget https://github.com/swiftlang/swift-driver/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-driver.tar.gz
fi
mkdir swift-driver
tar -C swift-driver -xzf ../swiftlang_$SWIFTTAG.orig-swift-driver.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-syntax.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-syntax.tar.gz"
else
    wget https://github.com/swiftlang/swift-syntax/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-syntax.tar.gz
fi
mkdir swift-syntax
tar -C swift-syntax -xzf ../swiftlang_$SWIFTTAG.orig-swift-syntax.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-yams.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-yams.tar.gz"
else
    wget https://github.com/jpsim/Yams/archive/$YAMS_VERSION.tar.gz
    mv -f $YAMS_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-yams.tar.gz
fi
mkdir yams
tar -C yams -xzf ../swiftlang_$SWIFTTAG.orig-yams.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-crypto.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-crypto.tar.gz"
else
    wget https://github.com/apple/swift-crypto/archive/refs/tags/$SWIFT_CRYPTO_VERSION.tar.gz
    mv -f $SWIFT_CRYPTO_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-crypto.tar.gz
fi
mkdir swift-crypto
tar -C swift-crypto -xzf ../swiftlang_$SWIFTTAG.orig-swift-crypto.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-atomics.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-atomics.zip"
else
    wget https://github.com/apple/swift-atomics/archive/refs/tags/$SWIFT_ATOMICS_VERSION.zip
    mv -f $SWIFT_ATOMICS_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-atomics.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-atomics.zip
mv swift-atomics-$SWIFT_ATOMICS_VERSION swift-atomics

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-stress-tester.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-stress-tester.tar.gz"
else
    wget https://github.com/swiftlang/swift-stress-tester/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-stress-tester.tar.gz
fi
mkdir swift-stress-tester
tar -C swift-stress-tester -xzf ../swiftlang_$SWIFTTAG.orig-swift-stress-tester.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-collections.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-collections.zip"
else
    wget https://github.com/apple/swift-collections/archive/refs/tags/$SWIFT_COLLECTIONS_VERSION.zip
    mv -f $SWIFT_COLLECTIONS_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-collections.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-collections.zip
mv swift-collections-$SWIFT_COLLECTIONS_VERSION swift-collections


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-numerics.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-numerics.zip"
else
    wget https://github.com/apple/swift-numerics/archive/refs/tags/$SWIFT_NUMERICS_VERSION.zip
    mv -f $SWIFT_NUMERICS_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-numerics.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-numerics.zip
mv swift-numerics-$SWIFT_NUMERICS_VERSION swift-numerics


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-system.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-system.zip"
else
    wget https://github.com/apple/swift-system/archive/refs/tags/$SWIFT_SYSTEM_VERSION.zip
    mv -f $SWIFT_SYSTEM_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-system.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-system.zip
mv swift-system-$SWIFT_SYSTEM_VERSION swift-system


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-docc.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-docc.tar.gz"
else
    wget https://github.com/swiftlang/swift-docc/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-docc.tar.gz
fi
mkdir swift-docc
tar -C swift-docc -xzf ../swiftlang_$SWIFTTAG.orig-swift-docc.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-lmdb.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-lmdb.tar.gz"
else
    wget https://github.com/swiftlang/swift-lmdb/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-lmdb.tar.gz
fi
mkdir swift-lmdb
tar -C swift-lmdb -xzf ../swiftlang_$SWIFTTAG.orig-swift-lmdb.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-docc-render-artifact.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-docc-render-artifact.tar.gz"
else
    wget https://github.com/swiftlang/swift-docc-render-artifact/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-docc-render-artifact.tar.gz
fi
mkdir swift-docc-render-artifact
tar -C swift-docc-render-artifact -xzf ../swiftlang_$SWIFTTAG.orig-swift-docc-render-artifact.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-docc-symbolkit.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-docc-symbolkit.tar.gz"
else
    wget https://github.com/swiftlang/swift-docc-symbolkit/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-docc-symbolkit.tar.gz
fi
mkdir swift-docc-symbolkit
tar -C swift-docc-symbolkit -xzf ../swiftlang_$SWIFTTAG.orig-swift-docc-symbolkit.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-markdown.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-markdown.tar.gz"
else
    wget https://github.com/swiftlang/swift-markdown/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-markdown.tar.gz
fi
mkdir swift-markdown
tar -C swift-markdown -xzf ../swiftlang_$SWIFTTAG.orig-swift-markdown.tar.gz --strip-components=1


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-llvm-bindings.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-llvm-bindings.tar.gz"
else
    wget https://github.com/swiftlang/swift-llvm-bindings/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-llvm-bindings.tar.gz
fi
mkdir swift-llvm-bindings
tar -C swift-llvm-bindings -xzf ../swiftlang_$SWIFTTAG.orig-swift-llvm-bindings.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-nio.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-nio.zip"
else
    wget https://github.com/apple/swift-nio/archive/refs/tags/$SWIFT_NIO_VERSION.zip
    mv -f $SWIFT_NIO_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-nio.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-nio.zip
mv swift-nio-$SWIFT_NIO_VERSION swift-nio


if [ -f "../swiftlang_$SWIFTTAG.orig-swift-nio-ssl.zip" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-nio-ssl.zip"
else
    wget https://github.com/apple/swift-nio-ssl/archive/refs/tags/$SWIFT_NIO_SSL_VERSION.zip
    mv -f $SWIFT_NIO_SSL_VERSION.zip ../swiftlang_$SWIFTTAG.orig-swift-nio-ssl.zip
fi
unzip -q ../swiftlang_$SWIFTTAG.orig-swift-nio-ssl.zip
mv swift-nio-ssl-$SWIFT_NIO_SSL_VERSION swift-nio-ssl

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-format.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-format.tar.gz"
else
    wget https://github.com/swiftlang/swift-format/archive/refs/tags/$SWIFT_FORMAT_VERSION.tar.gz
    mv -f $SWIFT_FORMAT_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-format.tar.gz
fi
mkdir swift-format
tar -C swift-format -xzf ../swiftlang_$SWIFTTAG.orig-swift-format.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-experimental-string-processing.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-experimental-string-processing.tar.gz"
else
    wget https://github.com/swiftlang/swift-experimental-string-processing/archive/swift-$SWIFTTAG-RELEASE.tar.gz
    mv -f swift-$SWIFTTAG-RELEASE.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-experimental-string-processing.tar.gz
fi
mkdir swift-experimental-string-processing
tar -C swift-experimental-string-processing -xzf ../swiftlang_$SWIFTTAG.orig-swift-experimental-string-processing.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-certificates.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-certificates.tar.gz"
else
    wget https://github.com/apple/swift-certificates/archive/$SWIFT_CERTIFICATES_VERSION.tar.gz
    mv -f $SWIFT_CERTIFICATES_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-certificates.tar.gz
fi
mkdir swift-certificates
tar -C swift-certificates -xzf ../swiftlang_$SWIFTTAG.orig-swift-certificates.tar.gz --strip-components=1

if [ -f "../swiftlang_$SWIFTTAG.orig-swift-asn1.tar.gz" ];
then
    echo "Found existing swiftlang_$SWIFTTAG.orig-swift-asn1.tar.gz"
else
    wget https://github.com/apple/swift-asn1/archive/$SWIFT_ASN1_VERSION.tar.gz
    mv -f $SWIFT_ASN1_VERSION.tar.gz ../swiftlang_$SWIFTTAG.orig-swift-asn1.tar.gz
fi
mkdir swift-asn1
tar -C swift-asn1 -xzf ../swiftlang_$SWIFTTAG.orig-swift-asn1.tar.gz --strip-components=1

cd ..

# Build "orig" tarball of all sources
echo "Building swiftlang_$SWIFTTAG.orig.tar.xz"
tar cfJ ../swiftlang_$SWIFTTAG.orig.tar.xz swiftlang-$SWIFTTAG
 rm -rf swiftlang-$SWIFTTAG
cd ..
rm -rf swift-repack
