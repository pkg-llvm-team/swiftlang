                            The SwiftCrypto Project
                            =======================

Please visit the SwiftCrypto web site for more information:

  * https://github.com/swiftlang/swift-crypto

Copyright 2019 The SwiftCrypto Project

The SwiftCrypto Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

Also, please refer to each LICENSE.<component>.txt file, which is located in
the 'license' directory of the distribution file, for the license terms of the
components that this product depends on.

-------------------------------------------------------------------------------

This product contains test vectors from Google's wycheproof project.

  * LICENSE (Apache License 2.0):
    * https://github.com/google/wycheproof/blob/master/LICENSE
  * HOMEPAGE:
    * https://github.com/google/wycheproof

---

This product contains a derivation of various scripts from SwiftNIO.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-nio



                            The SwiftPM Project
                            ====================

Please visit the SwiftPM web site for more information:

  * https://github.com/swiftlang/swift-package-manager

Copyright (c) 2014 - 2021 Apple Inc. and the Swift project authors

The Swift Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

Also, please refer to each LICENSE.txt file, which is located in
the 'license' directory of the distribution file, for the license terms of the
components that this product depends on.

-------------------------------------------------------------------------------

The dependency resolver is influenced by Dart project (pub tool).

  * LICENSE (BSD 3-Clause "New" or "Revised" License):
    * https://github.com/dart-lang/pub/blob/master/LICENSE
  * HOMEPAGE:
    * https://dart.dev
    * https://github.com/dart-lang/pub

---

This product contains a derivation of Vapor's JWTKit library, specifically `JWTParser.swift`, `JWTSerializer.swift` and `Base64URL.swift`.

  * LICENSE (MIT License):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://vapor.codes
    * https://github.com/vapor/jwt-kit

---

This product contains a derivation of OpenSSL's OCSP implementation, found under the `PackageCollectionsSigningLibc` module.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/openssl/openssl

---

The observability system is influenced by SwiftLog project.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-log

---

The observability system is influenced by SwiftMetrics project.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-metrics

---

The observability system is influenced by SwiftDistributedTracing project.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-distributed-tracing-baggage

---



                            The Swift-DocC Project
                            ======================

Please visit the Swift-DocC web site for more information:

  * https://github.com/swiftlang/swift-docc

Copyright (c) 2021 Apple Inc. and the Swift project authors

The Swift Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

-------------------------------------------------------------------------------

This product contains Swift NIO.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-nio

---

This product contains Swift NIO SSL.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-nio-ssl

---

This product contains Swift Crypto.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-crypto

---

This product contains Swift Argument Parser.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-argument-parser

---

This product contains a derivation of the cmark-gfm project, available at
https://github.com/swiftlang/swift-cmark.

  * LICENSE (BSD-2):
    * https://opensource.org/licenses/BSD-2-Clause
  * HOMEPAGE:
    * https://github.com/github/cmark-gfm

---

This product contains a derivation of the LMDB project, available at
https://github.com/swiftlang/swift-lmdb.

  * LICENSE (OpenLDAP License):
    * https://www.openldap.org/software/release/license.html
  * HOMEPAGE:
    * https://github.com/lmdb/lmdb
    


                            The Swift Markdown Project
                            ==========================

Please visit the Swift Markdown web site for more information:

  * https://github.com/swiftlang/swift-markdown

Copyright (c) 2021 Apple Inc. and the Swift project authors

The Swift Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

-------------------------------------------------------------------------------

This product contains Swift Argument Parser.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/swiftlang/swift-argument-parser

---

This product contains a derivation of the cmark-gfm project, available at
https://github.com/swiftlang/swift-cmark.

  * LICENSE (BSD-2):
    * https://opensource.org/licenses/BSD-2-Clause
  * HOMEPAGE:
    * https://github.com/github/cmark-gfm



                            The SwiftNIO Project
                            ====================

Please visit the SwiftNIO web site for more information:

  * https://github.com/swiftlang/swift-nio

Copyright 2017, 2018 The SwiftNIO Project

The SwiftNIO Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

Also, please refer to each LICENSE.<component>.txt file, which is located in
the 'license' directory of the distribution file, for the license terms of the
components that this product depends on.

-------------------------------------------------------------------------------

This product is heavily influenced by Netty.

  * LICENSE (Apache License 2.0):
    * https://github.com/netty/netty/blob/4.1/LICENSE.txt
  * HOMEPAGE:
    * https://netty.io

---

This product contains a derivation of the Tony Stone's 'process_test_files.rb'.

  * LICENSE (Apache License 2.0):
    * https://www.apache.org/licenses/LICENSE-2.0
  * HOMEPAGE:
    * https://github.com/tonystone/build-tools/commit/6c417b7569df24597a48a9aa7b505b636e8f73a1
    * https://github.com/tonystone/build-tools/blob/master/source/xctest_tool.rb

---

This product contains NodeJS's http-parser.

  * LICENSE (MIT):
    * https://github.com/nodejs/http-parser/blob/master/LICENSE-MIT
  * HOMEPAGE:
    * https://github.com/nodejs/http-parser

---

This product contains "cpp_magic.h" from Thomas Nixon & Jonathan Heathcote's uSHET

  * LICENSE (MIT):
    * https://github.com/18sg/uSHET/blob/master/LICENSE
  * HOMEPAGE:
    * https://github.com/18sg/uSHET

---

This product contains "sha1.c" and "sha1.h" from FreeBSD (Copyright (C) 1995, 1996, 1997, and 1998 WIDE Project)

  * LICENSE (BSD-3):
    * https://opensource.org/licenses/BSD-3-Clause
  * HOMEPAGE:
    * https://github.com/freebsd/freebsd/tree/master/sys/crypto

---

This product contains a derivation of Fabian Fett's 'Base64.swift'.

  * LICENSE (Apache License 2.0):
    * https://github.com/fabianfett/swift-base64-kit/blob/master/LICENSE
  * HOMEPAGE:
    * https://github.com/fabianfett/swift-base64-kit
